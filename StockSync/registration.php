<?php

/**
 * Syte_StockSync
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Syte_StockSync', __DIR__);
