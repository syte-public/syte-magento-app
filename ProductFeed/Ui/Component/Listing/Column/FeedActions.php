<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class FeedActions extends Column
{
    /** @const string */
    private const FEED_URL_PATH_EDIT = 'syteproductfeed/feed/edit';

    /** @const string */
    private const FEED_URL_PATH_DELETE = 'syteproductfeed/feed/delete';

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var string
     */
    private $editUrl;

    /**
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface       $urlBuilder
     * @param array              $components
     * @param array              $data
     * @param string             $editUrl
     *
     * @return void
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = [],
        $editUrl = self::FEED_URL_PATH_EDIT
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->editUrl = $editUrl;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param  array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $name = $this->getData('name');
                if (isset($item['feed_id'])) {
                    $id = $item['feed_id'];
                    $item[$name]['edit'] = [
                        'href' => $this->urlBuilder->getUrl($this->editUrl, ['feed_id' => $id]),
                        'label' => __('Edit')
                    ];
                    $title = __('Record') . ' #' . $id;
                    $item[$name]['delete'] = [
                        'href' => $this->urlBuilder->getUrl(self::FEED_URL_PATH_DELETE, ['feed_id' => $id]),
                        'label' => __('Delete'),
                        'confirm' => [
                            'title' => __('Delete History Record'),
                            'message' => __('Are you sure you want to delete the record #%1?', $id)
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
