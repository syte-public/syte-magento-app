<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Syte\ProductFeed\Model\ResourceModel\Feed as ResourceFeed;
use Syte\ProductFeed\Model\ResourceModel\Feed\CollectionFactory as FeedCollectionFactory;
use Syte\ProductFeed\Api\Data;
use Syte\ProductFeed\Api\Data\FeedInterface;
use Syte\ProductFeed\Api\FeedRepositoryInterface;

class FeedRepository implements FeedRepositoryInterface
{
    /**
     * @var ResourceFeed
     */
    protected $resource;

    /**
     * @var FeedFactory
     */
    protected $feedFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var dataFeedFactory
     */
    protected $dataFeedFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param ResourceFeed $resource
     * @param FeedFactory $feedFactory
     * @param Data\FeedInterfaceFactory $dataFeedFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     *
     * @return void
     */
    public function __construct(
        ResourceFeed $resource,
        FeedFactory $feedFactory,
        Data\FeedInterfaceFactory $dataFeedFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->feedFactory = $feedFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataFeedFactory = $dataFeedFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * Save entity
     *
     * @param FeedInterface $entity
     *
     * @return FeedInterface
     */
    public function save(FeedInterface $entity): FeedInterface
    {
        if (null === $entity->getStoreId()) {
            $storeId = $this->storeManager->getStore()->getId();
            $entity->setStoreId($storeId);
        }
        try {
            $this->resource->save($entity);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Cannot save the record: %1', $exception->getMessage()),
                $exception
            );
        }

        return $entity;
    }

    /**
     * Get entity by id
     *
     * @param int $id
     *
     * @return FeedInterface
     */
    public function getById(int $id): FeedInterface
    {
        $entity = $this->feedFactory->create();
        $entity->load($id);
        if (!$entity->getFeedId()) {
            throw new NoSuchEntityException(__('A record with id "%1" does not exist', $id));
        }

        return $entity;
    }

    /**
     * Delete entity
     *
     * @param FeedInterface $entity
     *
     * @return bool
     */
    public function delete(FeedInterface $entity): bool
    {
        try {
            $this->resource->delete($entity);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Cannot delete the record: %1',
                    $exception->getMessage()
                )
            );
        }

        return true;
    }

    /**
     * Delete entity by id
     *
     * @param int $id
     *
     * @return bool
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->getById($id));
    }
}
