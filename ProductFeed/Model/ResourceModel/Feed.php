<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace  Syte\ProductFeed\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;

class Feed extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('syte_product_feed', 'feed_id');
    }
}
