<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Model\Config\Source;

use Magento\Framework\Escaper;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Syte\Core\Model\Constants;

class ProductAttribute
{
    /** @const array */
    private const ATTRIBUTES_EXCLUDED = [];
    /** @const bool */
    public const ATTRIBUTES_AUTOSUGGEST = true;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * @param Escaper $escaper
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param AttributeRepositoryInterface $attributeRepository
     */
    public function __construct(
        Escaper $escaper,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        AttributeRepositoryInterface $attributeRepository
    ) {
        $this->escaper = $escaper;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Get list of product attributes.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [['value' => '', 'label' => self::ATTRIBUTES_AUTOSUGGEST ? '' : __('-- Please Select --')]];
        // product attributes
        $options = [];
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $attributeRepository = $this->attributeRepository->getList('catalog_product', $searchCriteria);
        foreach ($attributeRepository->getItems() as $item) {
            $code = $item->getAttributeCode();
            if (!in_array($code, self::ATTRIBUTES_EXCLUDED)) {
                if (!empty($item->getFrontendLabel()) && $label = $this->escaper->escapeQuote($item->getFrontendLabel())) {
                    $options[] = ['value' => $code, 'label' => $label . ' (' . $code . ')'];
                }
            }
        }
        usort($options, $this->callbackSorter('label'));
        $result[] = ['value' => $options, 'label' => __('Product Attributes')];
        // custom attributes
        $options = [];
        foreach (Constants::SYTE_PRODUCT_FEED_ATTRIBUTES as $code => $lbl) {
            if ( $code && is_string($lbl) && $label = $this->escaper->escapeQuote($lbl)) {
                $options[] = ['value' => $code, 'label' => Constants::SYTE_PRODUCT_FEED_ATTR_PREFIX . $label];
            }
        }
        usort($options, $this->callbackSorter('label'));
        $result[] = ['value' => $options, 'label' => __('Custom Attributes')];

        return $result;
    }

    /**
     * Sorter callback.
     *
     * @param string $key
     */
    private function callbackSorter(string $key)
    {
        return function ($a, $b) use ($key) {
            return strnatcmp($a[$key], $b[$key]);
        };
    }
}
