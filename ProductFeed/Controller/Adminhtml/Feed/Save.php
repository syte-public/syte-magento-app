<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Controller\Adminhtml\Feed;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Syte\ProductFeed\Model\Feed;
use Syte\ProductFeed\Model\FeedFactory;
use Syte\ProductFeed\Api\FeedRepositoryInterface;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var FeedFactory
     */
    private $feedFactory;

    /**
     * @var FeedRepositoryInterface
     */
    private $feedRepository;

    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param FeedFactory $feedFactory
     * @param FeedRepositoryInterface $feedRepository
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        FeedFactory $feedFactory,
        \Syte\ProductFeed\Api\FeedRepositoryInterface $feedRepository
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->feedFactory = $feedFactory;
        $this->feedRepository = $feedRepository;
        parent::__construct($context);
    }

    /**
     * Authorization level
     *
     * @see _isAllowed()
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Syte_ProductFeed::edit');
    }

    /**
     * Executable
     *
     * @return resultRedirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (empty($data['feed_id'])) {
                $data['feed_id'] = null;
            }
            $model = $this->feedFactory->create();
            if ($id = (int)$this->getRequest()->getParam('feed_id')) {
                try {
                    $model = $this->feedRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('These records no longer exist'));
                    return $resultRedirect->setPath('*/*/');
                }
            }
            $model->setData($data);
            $this->_eventManager->dispatch(
                'syteproductfeed_feed_prepare_save',
                ['feed' => $model, 'request' => $this->getRequest()]
            );
            try {
                $this->feedRepository->save($model);
                $this->messageManager->addSuccessMessage(__('The record has been successfully saved'));
                $this->dataPersistor->clear('syteproductfeed_feed');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['feed_id' => $model->getFeedId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the record'));
            }
            $this->dataPersistor->set('syteproductfeed_feed', $data);
            return $resultRedirect->setPath('*/*/edit', ['feed_id' => $this->getRequest()->getParam('feed_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
