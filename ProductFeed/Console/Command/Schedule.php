<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Syte\ProductFeed\Model\Helper;
use Syte\Core\Model\Constants;

class Schedule extends Command
{
    /**
     * @var Helper
     */
    private $helper;

    protected function configure()
    {
        $this->setName(Constants::SYTE_PRODUCT_FEED_BASE_COMMAND . ':' . Constants::SYTE_PRODUCT_FEED_MODE_COMMAND
            . ':schedule')
            ->setDescription(__('Execute scheduled features'))
            ->setDefinition($this->getMakerOptions());
    }

    /**
     * @param Helper $helper
     */
    public function __construct(Helper $helper)
    {
        $this->helper = $helper;
        parent::__construct();
    }

    /**
     * Configure oftions for import command
     *
     * @return array
     */
    protected function getMakerOptions(): array
    {
        return [];
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(__('Checking a schedule...'));
        $result = ($exitCode = $this->helper->executeExportBySchedule(true))
            ? __('Denied (exit code %1)', $exitCode)
            : __('Allowed');
        $output->writeln(__('Status') . ': ' . $result);
    }
}
