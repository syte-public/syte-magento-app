<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Api\Data;

interface FeedInterface
{
    /** @const string */
    public const FEED_ID = "feed_id";
    /** @const string */
    public const STORE_ID = "store_id";
    /** @const string */
    public const FILENAME = "filename";
    /** @const string */
    public const STATUS = "status";
    /** @const string */
    public const JOB_STARTED_AT = "job_started_at";
    /** @const string */
    public const CREATED_AT = "created_at";
    /** @const string */
    public const UPDATED_AT = "updated_at";

    /**
     * Get feed_id
     *
     * @return int
     */
    public function getFeedId(): int;

    /**
     * Get store_id
     *
     * @return int
     */
    public function getStoreId(): int;

    /**
     * Set store_id
     *
     * @param int $storeid
     *
     * @return $this
     */
    public function setStoreId(int $storeid);

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename(): string;

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return $this
     */
    public function setFilename(string $filename);

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus(): string;

    /**
     * Set status
     *
     * @param string $status
     *
     * @return $this
     */
    public function setStatus(string $status);

    /**
     * Get job_started_at
     *
     * @return string
     */
    public function getJobStartedAt(): string;

    /**
     * Set job_started_at
     *
     * @param string $jobStartedAt
     *
     * @return $this
     */
    public function setJobStartedAt(string $jobStartedAt);

    /**
     * Get created_at
     *
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * Get updated_at
     *
     * @return string
     */
    public function getUpdatedAt(): string;
}
