<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Block\Adminhtml;

class Feed extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_feed';
        $this->_blockGroup = 'Syte_ProductFeed';
        $this->_headerText = __('Manage Product Feed History');
        parent::_construct();
        $this->buttonList->remove('add');
    }

    /**
     * Authorization level
     *
     * @param string|int $resourceId
     *
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
