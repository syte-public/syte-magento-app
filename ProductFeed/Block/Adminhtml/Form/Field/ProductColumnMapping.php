<?php

/**
 * Syte_ProductFeed
 */

declare(strict_types=1);

namespace Syte\ProductFeed\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

class ProductColumnMapping extends AbstractFieldArray
{
    /**
     * @var ProductAttributeColumn
     */
    private $attrRenderer;

    /**
     * Prepare rendering the new field by adding all the needed columns
     */
    protected function _prepareToRender()
    {
        $this->addColumn('name', [
            'label' => __('Column Name'),
            'class' => 'required-entry syte-pf-column-m'
        ]);
        $this->addColumn('attribute', [
            'label' => __('Map To'),
            'renderer' => $this->getAttrRenderer(),
        ]);
        $this->addColumn('value', [
            'label' => __('Default'),
            'class' => 'syte-pf-column-m'
        ]);
        $this->addColumn('sort', [
            'label' => __('Order'),
            'class' => 'validate-not-negative-number syte-pf-column-s'
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add New Column');
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @throws LocalizedException
     *
     * @return void
     */
    protected function _prepareArrayRow(DataObject $row): void
    {
        $options = [];
        $attr = $row->getAttribute();
        if (null !== $attr) {
            $options['option_' . $this->getAttrRenderer()->calcOptionHash($attr)] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }

    /**
     * @return ProductAttributeColumn
     * @throws LocalizedException
     */
    private function getAttrRenderer()
    {
        if (!$this->attrRenderer) {
            $this->attrRenderer = $this->getLayout()->createBlock(
                ProductAttributeColumn::class,
                '',
                ['data' =>
                    [
                        'is_render_to_js_template' => true,
                        'cacheable' => false,
                        'custom_source_options' => $this->getElement()->getValues()
                    ]
                ]
            );
        }

        return $this->attrRenderer;
    }
}
