require(
    [
        'jquery',
        'Magento_Ui/js/modal/alert',
        'Magento_Ui/js/modal/confirm',
        'mage/translate',
        'jquery/validate',
        'Magento_Ui/js/modal/confirm'
        ],
    function ($, alert, confirmation, $t) {
        'use strict';

        const syte_ftp_validation_msg = 'syte-credentials-success-message';
        const syte_ftp_fields = '.syte-ftp';

        let setFtpValidateStatus = function (isValidated) {
            if ($('.' + syte_ftp_validation_msg)) {
                $('.' + syte_ftp_validation_msg).remove();
            }
            if (typeof(isValidated) != 'undefined' && isValidated !== null) {
                let btn = $('.syte-ftp-validation-button');
                if (isValidated) {
                    $('<div class="message message-success ' + syte_ftp_validation_msg + '">'
                        + $t("Connection has been established successfully") + '</div>').insertAfter(btn);
                } else {
                    $('<div class="message message-warning ' + syte_ftp_validation_msg + '">'
                        + $t('Connection failed') + '</div>').insertAfter(btn);
                }
            }
        }

        let showFtpValidationAlert = function (errorMsg) {
            setFtpValidateStatus(false);
            let txt = (errorMsg == '') ? $t('Connection to your host cannot be established. Please ensure you have entered a valid data.') : errorMsg;
            alert({
                title: $t('Connection Failed'),
                content: txt
            });
        }

        window.syteFtpValidator = function (endpoint) {
            let validator = $('#config-edit-form').validate();
            /* Remove previous success message if present */
            if ($('.' + syte_ftp_validation_msg)) {
                $('.' + syte_ftp_validation_msg).remove();
            }
            /* Basic field validation */
            let errors = [];
            if (!validator.element($(syte_ftp_fields))) {
                errors.push($t('Please enter valid values'));
            }
            if (errors.length > 0) {
                alert({
                    title: $t('Connection Settings Validation Failed'),
                    content:  errors.join('<br />')
                });
                setFtpValidateStatus(false);
                return false;
            }
            $(this).text($t("We're testing a connection...")).attr('disabled', true);
            let self = this;
            $.post(endpoint, {
                ftp_host: $('[data-ui-id="text-groups-product-feed-fields-ftp-host-value"]').val(),
                ftp_port: $('[data-ui-id="text-groups-product-feed-fields-ftp-port-value"]').val(),
                ftp_user: $('[data-ui-id="text-groups-product-feed-fields-ftp-user-value"]').val(),
                ftp_password: $('[data-ui-id="password-groups-product-feed-fields-ftp-password-value"]').val(),
                ftp_path: $('[data-ui-id="text-groups-product-feed-fields-ftp-path-value"]').val()
            }).done(function (response, textStatus, jqXHR) {
                if ((textStatus === 'success') && (typeof(response) != 'undefined') && (response !== null)) {
                    if (response.validated == true) {
                        setFtpValidateStatus(true);
                    } else {
                        showFtpValidationAlert(response.errormsg);
                    }
                } else {
                    showFtpValidationAlert('');
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                showFtpValidationAlert('');
            }).always(function () {
                $(self).text($t('Test Connection')).attr('disabled', false);
            });
        }

        $(document).ready(function () {
            setFtpValidateStatus();
            $(syte_ftp_fields).bind('change paste keyup', function (e) {
                setFtpValidateStatus();
            });
        });

        let showRunNowAlert = function (msg) {
            let txt = (msg == '') ? ($t('Errors have been detected.') + ' ' + $t('See logs for details.')) : msg;
            alert({
                title: $t('Job status'),
                content: txt
            });
        }

        window.syteRunNow = function (endpoint) {
            let btn = $('#syte-pf-run-now');
            confirmation({
                title: $.mage.__('Run Product Feed Export'),
                content: $.mage.__('Are you sure you want to dispatch the job? This may be time consuming and resource intensive.'),
                actions: {
                    confirm: function () {
                        btn.text($t('Executing a job...')).attr('disabled', true);
                        $.post(endpoint, {
                            action_mode: 'runnow'
                        }).done(function (response, textStatus, jqXHR) {
                            if ((textStatus === 'success') && (typeof(response) != 'undefined') && (response !== null)) {
                                showRunNowAlert((response.executed == true) ? response.statusmsg : '');
                            } else {
                                showRunNowAlert('');
                            }
                        }).fail(function (jqXHR, textStatus, errorThrown) {
                            showRunNowAlert('');
                        }).always(function () {
                            btn.text($t('Run Now')).attr('disabled', false);
                        });
                    },
                    cancel: function () {},
                    always: function () {}
                },
                buttons: [{
                    text: $.mage.__('Cancel'),
                    class: 'action-secondary action-dismiss',
                    click: function (event) {
                        this.closeModal(event);
                    }
                }, {
                    text: $.mage.__('OK'),
                    class: 'action primary action-accept',
                    click: function (event) {
                        this.closeModal(event, true);
                    }
                }]
            });
        }
    }
);
