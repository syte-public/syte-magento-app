<?php
declare(strict_types=1);

namespace Syte\Tracker\Plugin\Catalog\Block\Product;

use Magento\Catalog\Model\Product;
use Syte\Tracker\Helper\Product\ChildrenSkus\Data as Helper;
use Magento\Catalog\Block\Product\ListProduct;

/**
 * Plugin for ListProduct Block
 */
class ListProductPlugin
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * Data constructor
     *
     * @param Helper $helper
     */
    public function __construct(
        Helper $helper
    ) {
        $this->helper = $helper;
    }
    /**
     * Get product children skus html
     *
     * @param ListProduct $subject
     * @param string $result
     * @param Product $product
     *
     * @return string
     */
    public function afterGetProductDetailsHtml(
        ListProduct $subject,
        string $result,
        Product $product
    ): string {
        $result .= $this->helper->getChildrenSkusHtml($product);

        return $result;
    }
}
