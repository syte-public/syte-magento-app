<?php
declare(strict_types=1);

namespace Syte\Tracker\ViewModel;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;
use Syte\Tracker\Helper\Data;
use Psr\Log\LoggerInterface;

class Content implements ArgumentInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Content constructor
     *
     * @param Data $helper
     * @param StoreManagerInterface $storeManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        Data $helper,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger
    ) {
        $this->helper = $helper;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
    }

    /**
     * Get script code by key
     *
     * @param string $key
     *
     * @return string
     */
    public function getScriptCode(string $key): string
    {
        $data = [];
        try {
            $store = $this->storeManager->getStore();
            if ($store->getId()) {
                $data = $this->helper->getScriptContents((int)$store->getId());
            }
        } catch (NoSuchEntityException $e) {
            $errorMessage = sprintf("Can't get current store. Error %s.", $e->getMessage());
            $this->logger->error($errorMessage);
        }

        return $data[$key] ?? '';
    }
}
