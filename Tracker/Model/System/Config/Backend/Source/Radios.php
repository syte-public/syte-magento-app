<?php
declare(strict_types=1);

namespace Syte\Tracker\Model\System\Config\Backend\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Radios implements OptionSourceInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        $result = [];

        foreach ($this->toArray() as $key => $value) {
            $result[] = [
                'value' => $key,
                'label' => $value,
            ];
        }

        return $result;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
            'integration' => __('Visual Search'),
            'search' => __('Visual and Augmented Search'),
            'pixel' => __('Pixel'),
        ];
    }
}
