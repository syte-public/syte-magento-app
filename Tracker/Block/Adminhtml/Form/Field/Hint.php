<?php
declare(strict_types=1);

namespace Syte\Tracker\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Syte\Core\Model\Constants;

class Hint extends Field
{
    /**
     * @inheritDoc
     */
    protected function _renderScopeLabel(AbstractElement $element): string
    {
        // Return empty label
        return '';
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        $title = __('Anchors allowed:');
        $accountId = Constants::SYTE_SCRIPT_ANCHOR_ACCOUNT_ID . ' = ' . __('Account ID');
        $accountSignature = Constants::SYTE_SCRIPT_ANCHOR_ACCOUNT_SIG . ' = ' . __('Account Signature');
        $langCode = Constants::SYTE_SCRIPT_ANCHOR_LANG_CODE . ' = ' . __('Language Code (xx_XX)');
        // @codingStandardsIgnoreStart
        $html = <<<TEXT
            {$title}</br>{$accountId}</br>{$accountSignature}</br>{$langCode}
TEXT;
        // @codingStandardsIgnoreEnd

        return $html;
    }
}
