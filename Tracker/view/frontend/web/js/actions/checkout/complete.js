define([
    'jquery',
], function ($) {
    'use strict';

    /**
     * Dispatch checkout complete to Syte Tracker
     *
     * @param {Object} cart - cart data
     * @param {float} total - current cart total
     * @param {string} orderId - last order id
     * @param {string} currentCurrency - current currency
     *
     * @private
     */
    function send(cart, total, orderId, currentCurrency) {
        var syteData = {
            name: 'checkout_complete',
            tag: 'ecommerce',
            id: orderId,
            value: total,
            currency: currentCurrency,
            products: cart
        };
        window.syteDataLayer.push(syteData);
    }

    return function (data) {
        send(data.cart, data.cartTotal, data.orderId, data.currentCurrency)
    };
});

