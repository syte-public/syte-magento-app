<?php
declare(strict_types=1);

namespace Syte\Tracker\Helper\Product\ChildrenSkus;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Downloadable\Model\Product\Type as Downloadable;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\Catalog\Model\Product\Type as Simple;
use Magento\ConfigurableProduct\Api\LinkManagementInterface as ConfigurableLinkManagementInterface;
use Magento\Bundle\Api\ProductLinkManagementInterface as BundleLinkManagementInterface;
use Magento\Store\Model\StoreManagerInterface;
use Syte\Core\Model\Config;
use Psr\Log\LoggerInterface;

/**
 * Get children product skus
 */
class Data
{
    /**
     * @var ConfigurableLinkManagementInterface
     */
    private $configurableLinkManagement;

    /**
     * @var BundleLinkManagementInterface
     */
    private $bundleLinkManagement;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Data constructor
     *
     * @param ConfigurableLinkManagementInterface $configurableLinkManagement
     * @param BundleLinkManagementInterface $bundleLinkManagement
     * @param Config $config
     * @param StoreManagerInterface $storeManager
     * @param LoggerInterface $logger
     */
    public function __construct(
        ConfigurableLinkManagementInterface $configurableLinkManagement,
        BundleLinkManagementInterface $bundleLinkManagement,
        Config $config,
        StoreManagerInterface $storeManager,
        LoggerInterface $logger
    ) {
        $this->configurableLinkManagement = $configurableLinkManagement;
        $this->bundleLinkManagement = $bundleLinkManagement;
        $this->config = $config;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
    }

    /**
     * Get children products skus html
     *
     * @param ProductInterface $product
     *
     * @return string
     */
    public function getChildrenSkusHtml(ProductInterface $product): string
    {
        $result = '';

        try {
            $store = $this->storeManager->getStore();
            if ($store->getId()) {
                if ($this->config->isAccountActive((int)$store->getId())) {
                    switch ($product->getTypeId()) {
                        case Configurable::TYPE_CODE:
                            $children = $this->getConfigurableChildrenSkus($product);
                            break;
                        case Simple::TYPE_BUNDLE:
                            $children =  $this->getBundleChildrenSkus($product);
                            break;
                        case Grouped::TYPE_CODE:
                            $children =  $this->getGroupedChildrenSkus($product);
                            break;
                        case Downloadable::TYPE_DOWNLOADABLE:
                        case Simple::TYPE_SIMPLE:
                        case Simple::TYPE_VIRTUAL:
                            $children =  'variants=""';
                            break;
                        default:
                            $children =  'variants=""';
                    }

                    $result = sprintf(
                        '<div class="syte-product-group" masterID="%s" /%s></div>',
                        $product->getSku(),
                        $children
                    );
                }
            }
        } catch (NoSuchEntityException $e) {
            $errorMessage = sprintf("Can't get current store. Error %s.", $e->getMessage());
            $this->logger->error($errorMessage);
        }

        return $result;
    }

    /**
     * Create skus string from children products skus array
     *
     * @param array $childrenProducts
     *
     * @return string
     */
    private function createChildrenSkuString(array $childrenProducts): string
    {
        $childrenProductsSkus = [];
        if (!empty($childrenProducts)) {
            foreach ($childrenProducts as $childProduct) {
                $childrenProductsSkus[] = $childProduct->getSku();
            }
        }

        return implode(', ', $childrenProductsSkus);
    }

    /**
     * Get configurable product children skus
     *
     * @param ProductInterface $configurableProduct
     *
     * @return string
     */
    private function getConfigurableChildrenSkus(ProductInterface $configurableProduct): string
    {
        $childrenProducts = $this->configurableLinkManagement->getChildren($configurableProduct->getSku());
        $skus = $this->createChildrenSkuString($childrenProducts);

        return sprintf('child="%s"', $skus);
    }

    /**
     * Get bundle product children skus
     *
     * @param ProductInterface $bundleProduct
     *
     * @return string
     */
    private function getBundleChildrenSkus(ProductInterface $bundleProduct): string
    {
        try {
            $childrenProducts = $this->bundleLinkManagement->getChildren($bundleProduct->getSku());
            $skus = $this->createChildrenSkuString($childrenProducts);

            return sprintf('variants="%s"', $skus);
        } catch (InputException | NoSuchEntityException $e) {
            return 'variants=""';
        }
    }

    /**
     * Get grouped product children skus
     *
     * @param ProductInterface $groupedProduct
     *
     * @return string
     */
    private function getGroupedChildrenSkus(ProductInterface $groupedProduct): string
    {
        $childrenProductsInstance = $groupedProduct->getTypeInstance();
        $childrenProducts = $childrenProductsInstance->getAssociatedProducts($groupedProduct);
        $skus = $this->createChildrenSkuString($childrenProducts);

        return sprintf('variants="%s"', $skus);
    }
}
