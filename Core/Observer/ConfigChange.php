<?php
declare(strict_types=1);

namespace Syte\Core\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\App\RequestInterface;
use Syte\Core\Model\Events\Tracker;
use Magento\Framework\App\Config\Storage\WriterInterface;

class ConfigChange implements ObserverInterface
{
    public const XML_PATH_SYTE_CORE_FIRST_CONFIG = 'syte_core/account/first_config';

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Tracker
     */
    private $tracker;

    /**
     * @var WriterInterface
     */
    private $configWriter;

    private $fieldsMappings = [
                'enable_account' => 'active',
                'account_name' => 'name',
                'account_id' => 'id',
                'account_signature' => 'signature',
                'account_validated' => 'validated',
                'account_api_token' => 'api_token',
                'product_feed_name' => 'feed_name',
            ];

    /**
     * ConfigChange constructor.
     *
     * @param RequestInterface $request
     * @param Tracker $tracker
     * @param WriterInterface $configWriter
     */
    public function __construct(
        RequestInterface $request,
        Tracker $tracker,
        WriterInterface $configWriter
    ) {
        $this->request = $request;
        $this->tracker = $tracker;
        $this->configWriter = $configWriter;
    }

    /**
     * Track configuration changes
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $storeId = ($observer->getData('store') != '') ? $observer->getData('store') : 0;
        $changedPaths = $observer->getData('changed_paths');
        $changedFields = $this->getChangedFields($changedPaths);
        $groups = $this->request->getParam('groups');
        $firstConfigValue = $groups['account']['fields']['first_config']['value'] ?? null;
        $validatedAccount = $groups['account']['fields']['validated']['value'] ?? null;

        if ($validatedAccount) {
            if (!$firstConfigValue) {
                $configData = $this->createConfiguration(true, $changedFields);
                $this->tracker->configuration($configData, (int)$storeId);
                $this->configWriter->save(self::XML_PATH_SYTE_CORE_FIRST_CONFIG, '1');
            } else {
                $configData = $this->createConfiguration(false, $changedFields);
                if (!empty($configData['configuration'])) {
                    $this->tracker->configurationModified($configData, (int)$storeId);
                }
            }
        }
    }

    /**
     * Create tracking configuration array
     *
     * @param bool $isFirst
     * @param array $changedFields
     *
     * @return array
     */
    private function createConfiguration(bool $isFirst, array $changedFields): array
    {
        $configData = [];

        if ($isFirst) {
            $configData['configuration'] = $this->getSavedFieldsData();
        } else {
            $configData['configuration'] = $this->getChangedFieldsData($changedFields);
        }

        return $configData;
    }

    /**
     * Get array of the changed fields
     *
     * @param array $changedPaths
     *
     * @return array
     */
    private function getChangedFields(array $changedPaths): array
    {
        $clearChangedFields = str_replace('syte_core/account/', '', $changedPaths);

        return array_flip($clearChangedFields);
    }

    /**
     * Get array of the saved fields data
     *
     * @return array
     */
    private function getSavedFieldsData(): array
    {
        $savedFieldsData = [];
        $groups = $this->request->getParam('groups');
        foreach ($this->fieldsMappings as $syteName => $magentoName) {
            $savedFieldsData[$syteName] = $groups['account']['fields'][$magentoName]['value'] ?? null;
        }

        return $savedFieldsData;
    }

    /**
     * Get array of the changed fields data
     *
     * @param array $changedFields
     *
     * @return array
     */
    private function getChangedFieldsData(array $changedFields): array
    {
        $changedFieldsData = [];
        $groups = $this->request->getParam('groups');
        foreach ($this->fieldsMappings as $syteName => $magentoName) {
            if (isset($changedFields[$magentoName])) {
                $changedFieldsData[$syteName] = $groups['account']['fields'][$magentoName]['value'] ?? null;
            }
        }

        return $changedFieldsData;
    }
}
