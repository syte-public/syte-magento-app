<?php

/**
 * Syte_Core
 */

declare(strict_types=1);

namespace Syte\Core\Model;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Syte\Core\Model\Constants;

class Config extends AbstractHelper
{
    /**
     * @const string
     */
    private const SYTE_CONFIG_AREA = Constants::SYTE_CONFIG_PATH_ACCOUNT;

    /**
     * Get config value by fieldname
     *
     * @param string $field
     * @param null|int $storeId
     *
     * @return null|int|string
     */
    public function getConfigValue(string $field, $storeId = null)
    {
        return $this->scopeConfig->getValue($field, ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * Get config area value by short fieldname
     *
     * @param string $fieldShort
     * @param null|int $storeId
     *
     * @return null|int|string
     */
    private function getAreaConfig(string $fieldShort, $storeId = null)
    {
        return $this->getConfigValue(self::SYTE_CONFIG_AREA . $fieldShort, $storeId);
    }

    /**
     * Get account active status
     *
     * @param int $storeId
     *
     * @return bool
     */
    public function isAccountActive(int $storeId): bool
    {
        $isActive = (int)$this->getAreaConfig('active', $storeId);
        $isValidated = (int)$this->getAreaConfig('validated', $storeId);

        return $isActive && $isValidated;
    }

    /**
     * Get account name
     *
     * @param int $storeId
     *
     * @return string
     */
    public function getAccountName(int $storeId): string
    {
        return (string)$this->getAreaConfig('name', $storeId);
    }

    /**
     * Get account id
     *
     * @param int $storeId
     *
     * @return int
     */
    public function getAccountId(int $storeId): int
    {
        return (int)$this->getAreaConfig('id', $storeId);
    }

    /**
     * Get account signature
     *
     * @param int $storeId
     *
     * @return string
     */
    public function getAccountSignature(int $storeId): string
    {
        return (string)$this->getAreaConfig('signature', $storeId);
    }

    /**
     * Get account API Token
     *
     * @param int $storeId
     *
     * @return string
     */
    public function getAccountApiToken(int $storeId): string
    {
        return ""; // Not needed for now
        return (string)$this->getAreaConfig('api_token', $storeId);
    }

    /**
     * Get account product feed name
     *
     * @param int $storeId
     *
     * @return string
     */
    public function getAccountProductFeedName(int $storeId): string
    {
        return (string)$this->getAreaConfig('feed_name', $storeId);
    }
}
