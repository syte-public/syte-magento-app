<?php

/**
 * Syte_Core
 */

declare(strict_types=1);

namespace Syte\Core\Model;

class Constants
{
    public const SYTE_URL = 'https://www.syte.ai/';

    /* Account validation */
    public const SYTE_ACCOUNT_VALIDATION_URL = 'https://syteapi.com/et';
    public const SYTE_ACCOUNT_VALIDATION_DATA = ['tags' => 'syte_magento', 'name' => 'account_validated'];

    /* Admin config pathes */
    public const SYTE_CONFIG_PATH_ACCOUNT = 'syte_settings/account/';
    public const SYTE_CONFIG_PATH_SCRIPT = 'syte_settings/scripts/';
    public const SYTE_CONFIG_PATH_STOCK_SYNC = 'syte_settings/stock_sync/';
    public const SYTE_CONFIG_PATH_PRODUCT_FEED = 'syte_product_feed/product_feed/';
    public const SYTE_CONFIG_PATH_PRODUCT_FEED_FILE = 'syte_product_feed/product_feed/';

    /* Frontend script integration */
    public const SYTE_SCRIPT_INTEGRATION = 'Desktop and Mobile Web Integration';
    public const SYTE_SCRIPT_SEARCH = 'Visual and Augmented Search';
    public const SYTE_SCRIPT_PIXEL = 'Syte Pixel';

    /* Frontend script anchors */
    public const SYTE_SCRIPT_ANCHOR_ACCOUNT_ID = '[ACCOUNT_ID]';
    public const SYTE_SCRIPT_ANCHOR_ACCOUNT_SIG = '[ACCOUNT_SIGNATURE]';
    public const SYTE_SCRIPT_ANCHOR_LANG_CODE = '[LANGUAGE_CODE]';
    public const SYTE_SCRIPT_ANCHOR_FEED_NAME = '[FEED_NAME]';

    /* Types of remote connections */
    public const SYTE_CONFIG_CONNECTION_NONE = 0;
    public const SYTE_CONFIG_CONNECTION_SFTP = 1;

    /* Product Feed */
    public const SYTE_PRODUCT_FEED_FILE_EXTENSION = 'csv';
    public const SYTE_PRODUCT_FEED_PATH = 'export/syte/product_feed';
    public const SYTE_PRODUCT_FEED_COMPLETED_PATH = 'export/syte/product_feed/completed';
    public const SYTE_PRODUCT_FEED_BASE_COMMAND = 'syte';
    public const SYTE_PRODUCT_FEED_MODE_COMMAND = 'productfeed';

    /* Schedule mode */
    public const SYTE_SCHEDULE_NONE = 0;
    public const SYTE_SCHEDULE_ONCE = 1;
    public const SYTE_SCHEDULE_INTERVAL = 2;

    /* Product Feed customized attribures */
    public const SYTE_PRODUCT_FEED_ATTR_PARENT_SKU = 'syte_parent_sku';
    public const SYTE_PRODUCT_FEED_ATTR_PRODUCT_URL = 'syte_product_url';
    public const SYTE_PRODUCT_FEED_ATTR_IMAGE_URL = 'syte_image_url';
    public const SYTE_PRODUCT_FEED_ATTR_IMAGE_URL_1 = 'syte_image_url_1';
    public const SYTE_PRODUCT_FEED_ATTR_IMAGE_URL_2 = 'syte_image_url_2';
    public const SYTE_PRODUCT_FEED_ATTR_IMAGE_URL_3 = 'syte_image_url_3';
    public const SYTE_PRODUCT_FEED_ATTR_CATEGORY = 'syte_product_category';
    public const SYTE_PRODUCT_FEED_ATTR_QTY = 'syte_product_qty';
    public const SYTE_PRODUCT_FEED_ATTR_PRICE = 'syte_product_price';
    public const SYTE_PRODUCT_FEED_ATTR_CURRENCY = 'syte_product_currency';
    public const SYTE_PRODUCT_FEED_ATTR_LOCALE = 'syte_store_locale';
    public const SYTE_PRODUCT_FEED_ATTR_PRODUCT_TYPE = 'syte_product_type';
    public const SYTE_PRODUCT_FEED_ATTRIBUTES = [
        self::SYTE_PRODUCT_FEED_ATTR_PARENT_SKU => 'Parent SKU',
        self::SYTE_PRODUCT_FEED_ATTR_PRODUCT_URL => 'Product Page URL',
        self::SYTE_PRODUCT_FEED_ATTR_IMAGE_URL => 'Main Image URL',
        self::SYTE_PRODUCT_FEED_ATTR_IMAGE_URL_1 => 'Alternative Image URL (1)',
        self::SYTE_PRODUCT_FEED_ATTR_IMAGE_URL_2 => 'Alternative Image URL (2)',
        self::SYTE_PRODUCT_FEED_ATTR_IMAGE_URL_3 => 'Alternative Image URL (3)',
        self::SYTE_PRODUCT_FEED_ATTR_CATEGORY => 'Product Category',
        self::SYTE_PRODUCT_FEED_ATTR_QTY => 'Stock Quantity',
        self::SYTE_PRODUCT_FEED_ATTR_PRICE => 'Product Price',
        self::SYTE_PRODUCT_FEED_ATTR_CURRENCY => 'Currency',
        self::SYTE_PRODUCT_FEED_ATTR_LOCALE => 'Language Code',
        self::SYTE_PRODUCT_FEED_ATTR_PRODUCT_TYPE => 'Product Type',
    ];
    public const SYTE_PRODUCT_FEED_ATTR_PREFIX = '*';
}
